from BreakoutInspired.Game.Shared import GameObject
from BreakoutInspired.Game.Shared import GameConstants

class Brick(GameObject):  # Inherits from GameObject

   def __init__(self, position, sprite, game):
      self.__game = game

      super(Brick, self).__init__(position, GameConstants.BRICK_SIZE, sprite)

