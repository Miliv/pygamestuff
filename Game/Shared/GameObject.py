
class GameObject:

   def __init__(self, position, size, sprite):
      self.__position = position
      self.__size = size
      self.__sprite = sprite

   def setPosition(self, position):
      self.__position = position

   def getPosition(self):
      return self.__position

   def getSize(self):
      return self.__size

   def getSprite(self):
      return self.__sprite

   # def __intersectsY(self, other):
   #    if self.y >= other.y and self.y <= (other.y + other.height):
   #       return 1
   #    if (self.y + self.height) >= other.y and self.y + self.height <= (other.y + other.height):
   #       return 1
   #    return 0

   # def __intersectsX(self, other):
   #    if self.x >= other.x and self.x <= (other.x + other.width):
   #       return 1
   #    if (self.x + self.width) >= other.x and self.x + self.width <= (other.x + other.width):
   #       return 1
   #    return 0

   # def intersects(self, other):
   #    if self.__intersectsX(other) and self.__intersectsY(other):
   #       return 1
   #    return 0
