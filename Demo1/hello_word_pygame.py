import sys
import pygame


pygame.init()

windowSize = (800, 600)

screen = pygame.display.set_mode(windowSize)

myFont = pygame.font.SysFont("Myriad Pro", 48)

helloWorld = myFont.render("Hello World", 1, (255, 255, 60), (0, 0, 255))  
# text, anti alias, fcolor, bcolor


x, y = 0, 0
clock = pygame.time.Clock()
hwSize = helloWorld.get_size()

dirX = 1
dirY = 1


while 1:
   clock.tick(40)
   for event in pygame.event.get():
      if event.type == pygame.QUIT:
         sys.exit()

      if event.type == pygame.KEYDOWN:
         if event.key == pygame.K_RIGHT:
            x += 5
         if event.key == pygame.K_LEFT:
            x -= 5
         if event.key == pygame.K_UP:
            y += 5
         if event.key == pygame.K_DOWN:
            y -= 5

   # x += (5 * dirX)
   # y += (2 * dirY)

   if x + hwSize[0] > 800: x = 800 - hwSize[0]
   if x < 0: x = 0
   if y + hwSize[1] > 600: y = 600 - hwSize[1]
   if y < 0: y = 0
   
   

   screen.fill((0,0,0))
   screen.blit(helloWorld, (x, y))
   pygame.display.update()