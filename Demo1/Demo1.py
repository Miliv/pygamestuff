import sys
import pygame
# from pygame import pygame  # solo por el intellisense
from GameObject import GameObject


BLACK = (0,0,0)
WHITE = (255,255,255)

# Init PyGame
pygame.init()
# pygame.mixer.init()
pygame.mixer.pre_init(44100, 16, 2, 4096)


winSize = (800, 600)
screen = pygame.display.set_mode(winSize)
pygame.mouse.set_visible(0)

# Load resources
ball = pygame.image.load("BreakoutInspired/Ball.png")
sound = pygame.mixer.Sound("BreakoutInspired/Beep.wav")
font1 = pygame.font.SysFont("Myriad Pro", 48)
intersectText = font1.render("Intersecting!", 1, (255,0,255),(0,0,0))

# Prepare logo
ballSize = ball.get_size()
ball.fill(BLACK, None, pygame.BLEND_RGB_MAX)

x, y = 0, 0
clock = pygame.time.Clock()
dirX, dirY = 1, 1

def playSound():
   sound.stop()
   sound.play()

rectangle = GameObject(100, 100, 400, 400)
logo = GameObject(0, 0, ballSize[0], ballSize[1])

while 1:
   clock.tick(30)

   screen.fill(BLACK)

   for event in pygame.event.get():
      if event.type == pygame.QUIT: sys.exit()

   mousePos = pygame.mouse.get_pos()

   x = mousePos[0]
   y = mousePos[1]

   logo.setPosition(x, y)

   if logo.intersects(rectangle):
      screen.blit(intersectText, (10, 10))
      playSound()

   if x + ballSize[0] > 800: x = 800 - ballSize[0]
   if y + ballSize[1] > 600: y = 600 - ballSize[1]
   if x < 0: x = 0
   if y < 0: y = 0

   pygame.draw.rect(screen, WHITE, (100,100,400,400), 1)

   screen.blit(ball, (x, y))

   pygame.display.update()
